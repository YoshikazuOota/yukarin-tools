"""
extract indexes for alignment.
"""

import argparse
import glob
import multiprocessing
from functools import partial
from pathlib import Path
from pprint import pprint
from typing import Tuple

import librosa
import librosa.display

import numpy
import tqdm

from yukarin.acoustic_feature import AcousticFeature
from yukarin.align_indexes import AlignIndexes
from yukarin.param import AcousticParam
from yukarin.utility.json_utility import save_arguments
import matplotlib.pyplot as plot

base_acoustic_param = AcousticParam()

parser = argparse.ArgumentParser()
parser.add_argument('--input_feature_glob1', '-if1')
parser.add_argument('--input_feature_glob2', '-if2')
parser.add_argument('--input_indexes', '-ii')
parser.add_argument('--output', '-o', type=Path)
parser.add_argument('--sampling_rate', type=int, default=base_acoustic_param.sampling_rate)
parser.add_argument('--frame_period', type=float, default=base_acoustic_param.frame_period)
parser.add_argument('--alpha', type=float, default=base_acoustic_param.alpha)
parser.add_argument('--disable_overwrite', action='store_true')
arguments = parser.parse_args()
pprint(vars(arguments))

def flatten(data):
    for item in data:
        if hasattr(item, '__iter__'):
            for element in flatten(item):
                yield element
        else:
            yield item


def plot_acoustic_feature(align_indexes, fn):

    # make acoustic feature
    feature1 = align_indexes.get_aligned_feature1()
    f1_f0 = list(flatten(feature1.f0))

    feature2 = align_indexes.get_aligned_feature2()
    f2_f0 = list(flatten(feature2.f0))

    #wave1 = align_indexes.get_aligned_feature1().decode(sampling_rate=arguments.sampling_rate, frame_period=arguments.frame_period)
    #wave2 = align_indexes.get_aligned_feature2().decode(sampling_rate=arguments.sampling_rate, frame_period=arguments.frame_period)

    fig = plot.figure(figsize=(26, 26))
    #plot.subplot(2,1,1)
    #plot.title('speaker voice')
    #librosa.display.waveplot(wave1.wave, sr=arguments.sampling_rate)
    #plot.xlabel('')
    #plot.grid()

    #plot.subplot(2,1,2)
    plot.title('f0')
    plot.bar(range(len(f1_f0)), f1_f0, width=1.0)
    plot.grid()
    fig.savefig(str(arguments.output) + '/' + fn +'_own_plot.png')
    fig.clf()
    plot.close()

    fig = plot.figure(figsize=(26, 26))
    #plot.subplot(2,1,1)
    #plot.title('speaker voice')
    #librosa.display.waveplot(wave2.wave, sr=arguments.sampling_rate)
    #plot.xlabel('')
    #plot.grid()

    #plot.subplot(2,1,2)
    plot.title('f0')
    plot.bar(range(len(f2_f0)), f2_f0, width=1.0)
    plot.grid()
    fig.savefig(str(arguments.output) + '/' + fn +'_target_plot.png')
    fig.clf()
    plot.close()

def generate_aligned_wave(
        pair_path: Tuple[Path, Path, Path, str],
        sampling_rate: int,
        frame_period: float,
        alpha: float,
):
    path_feature1, path_feature2, path_indexes, fine_name = pair_path

    if path_feature1.stem != path_feature2.stem:
        print('warning: the file names are different', path_feature1, path_feature2)

    if path_feature1.stem != path_indexes.stem:
        print('warning: the file names are different', path_feature1, path_indexes)

    if arguments.disable_overwrite:
        return

    feature1 = AcousticFeature.load(path=path_feature1)
    feature2 = AcousticFeature.load(path=path_feature2)
    feature1.sp = AcousticFeature.mc2sp(feature1.mc, sampling_rate=sampling_rate, alpha=alpha)
    feature2.sp = AcousticFeature.mc2sp(feature2.mc, sampling_rate=sampling_rate, alpha=alpha)
    feature1.ap = AcousticFeature.decode_ap(feature1.coded_ap, sampling_rate=sampling_rate)
    feature2.ap = AcousticFeature.decode_ap(feature2.coded_ap, sampling_rate=sampling_rate)

    align_indexes = AlignIndexes.load(path=path_indexes)
    align_indexes.feature1 = feature1
    align_indexes.feature2 = feature2

    wave1 = align_indexes.get_aligned_feature1().decode(sampling_rate=sampling_rate, frame_period=frame_period)
    wave2 = align_indexes.get_aligned_feature2().decode(sampling_rate=sampling_rate, frame_period=frame_period)

    # save
    #y = numpy.vstack([wave1.wave, wave2.wave])
    out1 = Path(arguments.output, path_indexes.stem + '_own.wav')
    out2 = Path(arguments.output, path_indexes.stem + '_target.wav')
    librosa.output.write_wav(str(out1), wave1.wave, sr=sampling_rate)
    librosa.output.write_wav(str(out2), wave2.wave, sr=sampling_rate)

    plot_acoustic_feature(align_indexes, fine_name)


def main():
    arguments.output.mkdir(exist_ok=True)
    save_arguments(arguments, arguments.output / 'arguments.json')

    path_feature1 = {Path(p).stem: Path(p) for p in glob.glob(arguments.input_feature_glob1)}
    path_feature2 = {Path(p).stem: Path(p) for p in glob.glob(arguments.input_feature_glob2)}

    path_indexes = {Path(p).stem: Path(p) for p in glob.glob(arguments.input_indexes)}
    fn_both_list = set(path_feature1.keys()) & set(path_indexes.keys())


    for index, fn in enumerate(fn_both_list):
        print('%i / %i : %s' % (index+1, len(fn_both_list), fn) )
        generate_aligned_wave(
            pair_path=[path_feature1[fn],path_feature2[fn], path_indexes[fn], fn],
            sampling_rate=arguments.sampling_rate,
            frame_period=arguments.frame_period,
            alpha=arguments.alpha,
        )
    '''
    pool = multiprocessing.Pool()
    generate = partial(
        generate_aligned_wave,
        sampling_rate=arguments.sampling_rate,
        frame_period=arguments.frame_period,
        alpha=arguments.alpha,
    )
    it = pool.imap(generate, ((path_feature1[fn], path_feature2[fn], path_indexes[fn], fn) for fn in fn_both_list))
    list(tqdm.tqdm(it, total=len(path_feature1)))
    '''

if __name__ == '__main__':
    main()
