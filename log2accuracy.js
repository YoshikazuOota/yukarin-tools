#!/usr/bin/env node

const _s = require("underscore.string");

const commandLineArgs = require('command-line-args');
const _ = require('underscore');

const optionDefinitions = [
    { name: 'acc',  type: Number, defaultValue: 0.85 },
    { name: 'src',  type: String, multiple: false, defaultOption: true },
    { name: 'skip', type: Number, defaultValue: 5000 },
    { name: 'full', type: Boolean, defaultValue: false }
];
const options = commandLineArgs(optionDefinitions);
console.log(options);

// 引数チェック
if ( options.src == undefined ) {
    console.log("Please select log file");                                     
    process.exit(1);
}

var fs = require("fs");
var obj = JSON.parse(fs.readFileSync(options.src, "utf8").replace(/\bNaN\b/g, "null"));

var show_log = function (element ) {

    if(element["discriminator/accuracy"] > options.acc) {

        const train_acc  = parseFloat(element["discriminator/accuracy"]).toFixed(2);
        const train_loss = parseFloat(element["discriminator/loss"]).toFixed(3);
        const test_acc   = parseFloat(element["test/discriminator/accuracy"]).toFixed(2);
        //const test_loss  = parseFloat(element["test/loss"]).toFixed(4);

        const time = element["elapsed_time"];
        const time_hour = parseInt(time / 60 / 60);
        const time_min = parseInt(time / 60) -  time_hour * 60;
        const time_sec = parseInt(time - (time_min * 60 + time_hour * 60 * 60 ));

        const time_hour_s = _s.lpad(time_hour, 2, " ");
        const time_min_s =  _s.lpad(time_min, 2, "0");
        const time_sec_s =  _s.lpad(time_sec, 2, "0");

        console.log(`- ${_s.lpad(element.iteration, 6, ' ')} :: train > acc : ${train_acc}, loss ${train_loss} :: test > acc ${test_acc} (${time_hour_s}:${time_min_s}:${time_sec_s})`);
    }
};

obj.forEach(function(element) {

    if(options.full) {
        show_log(element);
    } else {
        if (element.iteration % options.skip === 0) {
            show_log(element);
        }
    }
});
