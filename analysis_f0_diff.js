#!/usr/bin/env node

const commandLineArgs = require('command-line-args');
const _s = require("underscore.string");
const _ = require('underscore');
const ss = require('simple-statistics');
const ervy = require('ervy');
const { bar, pie, bullet, donut, gauge, scatter } = ervy;

const optionDefinitions = [
    {name: 'src',  type: String, multiple: false, defaultOption: true},
    {name: 'threshold', alias: 't',  type: Number},
    {name: 'del_command', alias: 'c',  type: String},
    {name: 'del_file', alias: 'f',  type: Boolean},
];
const options = commandLineArgs(optionDefinitions);

if(!option_checker(options)) process.exit(-1);
//console.log(`debug : ${JSON.stringify(options)}\n`);

function plot_arrary(array){
    array.forEach( data => {
        data.key = `${data.key} ${parseInt((data.value))} :`;
    });
    console.log(' ' + bullet(array, {padding: 0, width: 100, style: '#'}));
}

function print_statistics(array){
    const order = 12;
    console.log(`length: ${_s.pad(array.length.toLocaleString(), order - 3)}`);
    console.log(`min: ${_s.pad(parseInt(ss.min(array)).toLocaleString(), order)}`);
    console.log(`max: ${_s.pad(parseInt(ss.max(array)).toLocaleString(), order)}`);
    console.log(`sum: ${_s.pad(parseInt(ss.sumSimple(array)).toLocaleString(), order)}`);
    console.log(`ave: ${_s.pad(parseInt(ss.mean(array)).toLocaleString(), order)}`);
    console.log(`sigma: ${_s.pad(parseInt(Math.sqrt(ss.variance(array))).toLocaleString(), order - 2)}`);
}

function option_checker (options){
    let return_bool = true;

    if(_.isEmpty(options.src)){
        console.log('json ファイル指定しねてね');
        return_bool = false;
    }
    return return_bool;
}
if(!option_checker(options)) process.exit(-1);

let f0_diff_datas = require(`${process.cwd()}/${options.src}`);

// 閾値があればフィルタリング
f0_diff_datas_ok = _.filter( f0_diff_datas, data => {
    return !options.threshold || (parseInt(data.value) < options.threshold);
});

if(_.isEmpty(f0_diff_datas_ok)) {
    console.log('empty');
    process.exit(-1);
}

let diff_counts = [];
f0_diff_datas_ok.forEach( data => {
    diff_counts.push(parseFloat(data.value));
});

let diff_counts_ng = [];
let f0_diff_datas_ng;

if(options.threshold) {
    f0_diff_datas_ng = _.filter(f0_diff_datas, data => {
        return !options.threshold || (parseInt(data.value) >= options.threshold);
    });

    f0_diff_datas_ng.forEach(data => {
        diff_counts_ng.push(parseFloat(data.value));
    });
}

if (!_.isEmpty(f0_diff_datas_ng) && options.del_command) {
    console.log(`rm ${options.del_command}/aligned_wav_split/all_diff.json`);
    f0_diff_datas_ng.forEach(data => {
        console.log(`rm ${options.del_command}/aligned_wav_split/${data.key}_diff_plot.jpeg`);
        console.log(`rm ${options.del_command}/aligned_wav_split/${data.key}_own.wav`);
        console.log(`rm ${options.del_command}/aligned_wav_split/${data.key}_target.wav`);
        console.log(`rm ${options.del_command}/aligned_wav_split/${data.key}_own_plot.png`);
        console.log(`rm ${options.del_command}/aligned_wav_split/${data.key}_target_plot.png`);
        console.log(`rm ${options.del_command}/aligned_indexes/${data.key}.npy`);
        console.log(`rm ${options.del_command}/npy_pair/target/${data.key}.npy`);
        console.log(`rm ${options.del_command}/npy_pair/own/${data.key}.npy`);

        console.log(`rm ${options.del_command}/voice_pair/target/${data.key}.wav`);
        console.log(`rm ${options.del_command}/voice_pair/own/${data.key}.wav`);
    });
    process.exit(0);
}

if (!_.isEmpty(f0_diff_datas_ng) && options.del_file) {
    plot_arrary(f0_diff_datas_ng);
    console.log('-------------------------------------------');
    console.log('NG statistics');
    print_statistics(diff_counts_ng);

    process.exit(0);
}

plot_arrary(f0_diff_datas_ok);
console.log('-------------------------------------------');
console.log('OK statistics');
print_statistics(diff_counts);
