#!/usr/bin/env node

const commandLineArgs = require('command-line-args');
const _s = require("underscore.string");
const _ = require('underscore');
const { exec, execSync } = require('child_process');
const utils = require('./js_lib/tool_utils');
const fs = require("fs");

const file_extension = 'png';
const diff_extension = '_diff_plot.jpeg';
const own_extension = '_own_plot.png';
const target_extension = '_target_plot.png';

const tmp_file = '/tmp/command4generate_diff_images.sh';

const optionDefinitions = [
    {name: 'src',  type: String, multiple: false, defaultOption: true}
];
const options = commandLineArgs(optionDefinitions);

console.log(`debug : ${JSON.stringify(options)}\n`);

function option_checker (options){
    let return_bool = true;

    // 引数チェック
    if(utils.isExistDirWithMsg(options.src, file_extension)){
        if(utils.isEmptyDir(options.src, file_extension)){
            console.log('warning: file is nothing');
            console.log('warning: チェック対象のディレクトリに png が無いよ');
            return_bool = false
        }
    }
    return return_bool;
}

if(!option_checker(options)) process.exit(1);

const png_files = utils.dirFiles(options.src, file_extension);

let voice_labels = _.map( png_files, file_name => {
    return file_name.replace(own_extension, '').replace(target_extension, '').replace(diff_extension, '');
});

voice_labels = _.uniq(voice_labels);
//console.log(voice_labels);

let commands = '';
voice_labels.forEach( (label, i) => {
    const each = `echo '${i} / ${voice_labels.length}'`;
    const command = `composite -compose difference ${process.cwd()}/${options.src}/${label}${own_extension} ${process.cwd()}/${options.src}/${label}${target_extension} ${process.cwd()}/${options.src}/${label}${diff_extension}`;
    commands += `${each}\n`;
    commands += `${command}\n`;

    //const command = `compare -metric AE ${process.cwd()}/${options.src}/${label}${own_extension} ${process.cwd()}/${options.src}/${label}${target_extension} ${process.cwd()}/${options.src}/${label}${diff_extension}`
    //const stdout = execSync(command, {encoding: 'utf8'}).toString();

    //console.log(command);
    //console.log(stdout);
});


fs.writeFile(tmp_file, commands, e => console);

console.log(`please run following command by copy & paste`);
console.log(`sh ${tmp_file}`);
