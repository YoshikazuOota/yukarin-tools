#!/usr/bin/env bash

for FILE in dat/in_myVoice/*.wav
do
    FILENAME=`echo ${FILE} | sed 's!^.*/!!'`
    echo ${FILENAME}
    ffmpeg -i dat/in_myVoice/${FILENAME} -i dat/in_yukariVoice/${FILENAME} -filter_complex amix=inputs=2:duration=longest:dropout_transition=2 dat/in_marge/${FILENAME}
done