#!/usr/bin/env node

const commandLineArgs = require('command-line-args');
const _s = require("underscore.string");
const _ = require('underscore');
const { execSync } = require('child_process');
const utils = require('./js_lib/tool_utils');

const file_extension = 'wav';

const optionDefinitions = [
    {name: 'src',  type: String, multiple: false, defaultOption: true},
    {name: 'sampling', alias: 's',  type: Number}
 ];
const options = commandLineArgs(optionDefinitions);

console.log(`debug : ${JSON.stringify(options)}\n`);

function option_checker (options){
    let return_bool = true;

    // 引数チェック
    if(utils.isExistDirWithMsg(options.src, file_extension)){
        if(utils.isEmptyDir(options.src, file_extension)){
            console.log('warning: file is nothing');
            console.log(`warning: チェック対象のディレクトリに ${file_extension} が無いよ`);
            return_bool = false
        }
    }

    if (!options.sampling) {
        console.log("error: please set '-s' to check sampling");
        console.log("エラー: '-s' で チェックしたいサンプリングレートを指定してね(例 -s 44100 )");
        return_bool = false;
    }

    return return_bool;
}

if(!option_checker(options)) process.exit(1);

const check_list = [
    { name: 'channels',       option: '-c', value: 1 },
    { name: 'bits per sample',option: '-b', value: 16},
    { name: 'encoding',       option: '-e', value: 'Signed Integer PCM' },
    { name: 'sampling',       option: '-r', value: options.sampling }
];

let success_flag = true;

const wav_files = utils.dirFiles(options.src, file_extension);
wav_files.forEach( file => {
    check_list.forEach( check => {
        const file_path = `${options.src}/${file}`;
        const stdout = execSync(`soxi ${check.option} ${file_path}`, {encoding: 'utf8'});

        if((check.value != stdout.trim())) {
            console.log(file_path);
            console.log(`error: ${check.name} is not ${check.value}`);
            success_flag = false;
        }
    });
});

if(success_flag) {
    console.log('all files format :');
    check_list.forEach(check => console.log(` > ${check.name} : ${check.value}`));
    console.log('\nresult : OK');
} else {
    console.log('\nresult : NG');
}



