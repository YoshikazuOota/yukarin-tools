#!/usr/bin/env bash

for FILE in dat/in_yukariVoice_44100/*.wav
do
    FILENAME=`echo ${FILE} | sed 's!^.*/!!'`
    echo ${FILENAME}
    sox dat/in_yukariVoice_22050/${FILENAME} -D dat/in_yukariVoice_44100/${FILENAME} rate 44100
done