#!/usr/bin/env node

const commandLineArgs = require('command-line-args');
const _s = require("underscore.string");
const _ = require('underscore');
const { execSync } = require('child_process');
const utils = require('./js_lib/tool_utils');

const file_extension = '*';

const optionDefinitions = [
    {name: 'src_dir',  alias: 'b', type: String},
    {name: 'dist_dir', alias: 'd', type: String},
];
const options = commandLineArgs(optionDefinitions);

console.log(`debug : ${JSON.stringify(options)}\n`);

function option_checker (options){
    let return_bool = true;

    if(!utils.isExistDirWithMsg(options.src_dir, file_extension)){
        return_bool = false;
    }

    if(!utils.isExistDirWithMsg(options.dist_dir, file_extension)){
        return_bool = false;
    }

    return return_bool;
}

if(!option_checker(options)) process.exit(1);


const src_files = utils.dirFiles(options.src_dir, file_extension);
const dist_files = utils.dirFiles(options.dist_dir, file_extension);

console.log(`-b dir files count: ${src_files.length}`);
console.log(`-d dir files count: ${dist_files.length}`);

if(src_files.length === dist_files.length) {
    console.log('\nresult : OK');
} else {
    console.log('\nresult : NG');
}