import argparse
from pathlib import Path

import shutil

import librosa
import time
import json

from become_yukarin import AcousticConverter
from become_yukarin import SuperResolution
from become_yukarin import VoiceChanger
from become_yukarin import Vocoder

from become_yukarin.param import AcousticFeatureParam

from become_yukarin.config.config import create_from_json as create_config_1st
from become_yukarin.config.sr_config import create_from_json as create_config_2nd

parser = argparse.ArgumentParser()
parser.add_argument('wav_file', nargs='+')
args = parser.parse_args()

setting = json.load(open(Path("./myscript/setting.json")))

gpu = setting["gpu"]
CONFIG_1ST_PATH = Path(setting["model"]["1st_conf"])
MODEL_1ST_PATH  = Path(setting["model"]["1st_model"])
CONFIG_2ND_PATH = Path(setting["model"]["2nd_conf"])
MODEL_2ND_PATH  = Path(setting["model"]["2nd_model"])

if args.wav_file is not None:
    config_1st = create_config_1st(CONFIG_1ST_PATH)
    config_2nd = create_config_2nd(CONFIG_2ND_PATH)

    acoustic_converter = AcousticConverter(config_1st, MODEL_1ST_PATH, gpu=gpu)
    super_resolution = SuperResolution(config_2nd, MODEL_2ND_PATH, gpu=gpu)

    output = Path('./output').absolute() / 'myscript'
    output.mkdir(exist_ok=True)

    shutil.copyfile("./myscript/setting.json", str(output) + "/setting.json")

    vocoder = Vocoder(
      acoustic_feature_param = AcousticFeatureParam(),
      out_sampling_rate = config_1st.dataset.param.voice_param.sample_rate,
    )

    # 低解像度周波数信号からの復元
    voice_changer = VoiceChanger(
        acoustic_converter=acoustic_converter,
        super_resolution=super_resolution,
        vocoder = vocoder
    )

    for p_str in args.wav_file:
        start_time = time.time()

        p = Path(p_str)
        wave = voice_changer.convert_from_wave_path(p)
        librosa.output.write_wav(str(output / p.stem) + '.wav', wave.wave, config_1st.dataset.param.voice_param.sample_rate, norm=True)

        print(p_str  + " : " +  format(time.time() - start_time))





