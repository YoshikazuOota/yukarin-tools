#!/usr/bin/env node

const commandLineArgs = require('command-line-args');
const _s = require("underscore.string");
const _ = require('underscore');
const { execSync } = require('child_process');
const utils = require('./js_lib/tool_utils');

const file_extension = 'wav';

const optionDefinitions = [
    {name: 'src_dir',  alias: 'b', type: String},
    {name: 'dist_dir', alias: 'd', type: String},
    {name: 'sampling', alias: 's', type: Number}
];
const options = commandLineArgs(optionDefinitions);

console.log(`debug : ${JSON.stringify(options)}\n`);

function option_checker (options){
    let return_bool = true;

    if(utils.isExistDirWithMsg(options.src_dir, file_extension)){
        if(utils.isEmptyDir(options.src_dir, file_extension)){
            console.log('warning: file is nothing');
            console.log('warning: チェック対象のディレクトリに wav が無いよ');
            return_bool = false;
        }
    }

    if(utils.isExistDir(options.dist_dir)) {
        // ディレクトリがあれば、空であるかチェック
        if(!utils.isEmptyDir(options.dist_dir, file_extension)){
            console.log('warning: file is not empty');
            console.log('warning: チェック対象のディレクトリは空にしてね。(上書き防止)');
            return_bool = false;
        }

    } else {
        // ディレクトリがなければ、階層ごとディレクトリ作成
        console.log(`make dist dir : ${options.dist_dir}`);
        utils.mkdirs(options.dist_dir);
    }
    return return_bool;
}

// オプションのチェック
if(!option_checker(options)) process.exit(1);

const wav_files = utils.dirFiles(options.src_dir, file_extension);
wav_files.forEach( file => {
    const src_file_path = `${options.src_dir}/${file}`;
    const dist_dir = `${options.dist_dir}/${file}`;
    const command = `sox ${src_file_path} -r ${options.sampling} ${dist_dir}`;

    console.log(command);
    const stdout = execSync(`${command}`, {encoding: 'utf8'});
});





