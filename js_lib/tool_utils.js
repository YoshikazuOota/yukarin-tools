const fs = require('fs-extra');
const _ = require('underscore');

module.exports = {
    dirFiles : function(dir, extension) {
        let file_list = [];
        const all_fils_list = fs.readdirSync(dir);
        const extension_matcher = new RegExp(`.*\.${extension}`);
        file_list = all_fils_list.filter(file => {
            return fs.statSync(`${dir}/${file}`).isFile() && extension_matcher.test(file);
        });
        return file_list;
    },
    isExistDir : function (dir) {
        try {
            return fs.statSync(dir) && fs.statSync(dir).isDirectory();
        } catch(err) {
            if (err.code === 'ENOENT') {
                return false;
            }
        }
    },
    isEmptyDir :function (dir, extension) {
        const files = this.dirFiles(dir, extension);
        return _.isEmpty(files)
    },
    isExistDirWithMsg: function (dir, extension) {
        let return_bool = true;

        if (!dir) {
            console.log("error: please set argument of source directory");
            console.log("エラー: 引数にサンプリングチェックしたいディレクトリを指定してね");
            return_bool = false;
        } else {
            if (!this.isExistDir(dir)) {
                console.log("error: src directory is nothing, or src is file");
                console.log("error: チェック対象のディレクトリが無い。または、ファイルだから確認してね");
                return_bool = false;
            }
        }
        return return_bool;
    },
    mkdirs: function (dir_path) {
        fs.mkdirsSync(dir_path)
    }
};