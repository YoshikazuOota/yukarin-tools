#!/bin/bash

mkdir dat
mkdir dat/1st_models
mkdir dat/2nd_models

mkdir dat/input
mkdir dat/output

mkdir dat/voice_src
mkdir dat/voice_src/voice_22050
mkdir dat/voice_src/voice_44100
mkdir dat/voice_src/voice_44800

mkdir dat/voice_src/voice_44100/yukari_pair
mkdir dat/voice_src/voice_44100/yukari_pair/own
mkdir dat/voice_src/voice_44100/yukari_pair/target
