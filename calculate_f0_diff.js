#!/usr/bin/env node

const commandLineArgs = require('command-line-args');
const _s = require("underscore.string");
const _ = require('underscore');
const { exec, execSync } = require('child_process');
const utils = require('./js_lib/tool_utils');
const fs = require("fs");

const file_extension = 'png';
const diff_extension = '_diff_plot.jpeg';
const own_extension = '_own_plot.png';
const target_extension = '_target_plot.png';

const tmp_file = '/tmp/check_f0_diff____.sh';

const optionDefinitions = [
    {name: 'src',  type: String, multiple: false, defaultOption: true}
];
const options = commandLineArgs(optionDefinitions);

console.log(`debug : ${JSON.stringify(options)}\n`);

function option_checker (options){
    let return_bool = true;

    // 引数チェック
    if(utils.isExistDirWithMsg(options.src, file_extension)){
        if(utils.isEmptyDir(options.src, file_extension)){
            console.log('warning: file is nothing');
            console.log('warning: チェック対象のディレクトリに png が無いよ');
            return_bool = false
        }
    }
    return return_bool;
}

if(!option_checker(options)) process.exit(1);

const png_files = utils.dirFiles(options.src, file_extension);

let voice_labels = _.map( png_files, file_name => {
    return file_name.replace(own_extension, '').replace(target_extension, '').replace(diff_extension, '');
});

let calculate_diff = [];

voice_labels = _.uniq(voice_labels);

/* debug
voice_labels = [
    'v_art001_sjis',
    'v_art002_sjis',
    'v_art003_sjis',
    'v_art004_sjis',
    'v_art005_sjis',
    'v_art006_sjis',
    'v_art007_sjis',
    'v_art008_sjis',
    'v_art009_sjis',
    'v_art010_sjis',
];
*/

voice_labels.forEach( (label, index)=> {
    const file_path = `${options.src}/${label}${diff_extension}`;
    const command = `identify -format "%[mean]" ${file_path}`;
    const stdout = execSync(command, {encoding: 'utf8'}).toString();
    console.log(`${index}/${voice_labels.length}: ${label} -> ${stdout}`);

    calculate_diff.push({key: label, value: stdout});
});

console.log(calculate_diff);

fs.writeFileSync(`${options.src}/all_diff.json`, JSON.stringify(calculate_diff));
